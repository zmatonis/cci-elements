============
CERN Ironic rsyslog configuration
============

This element is supposed to configure rsyslog to redirect all messages coming from ironic-python-agent to /dev/console

.. _ironic-agent: https://github.com/openstack/diskimage-builder/tree/master/diskimage_builder/elements/ironic-agent
